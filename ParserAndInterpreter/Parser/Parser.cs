using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public static class Parser
    {
        private static void HandleOperator(Stack<IEvaluable> expressionStack, Stack<Operator> opStack, 
            ExpressionTokenizer.Token? token, ExpressionTokenizer.Token? prevToken)
        {
            bool isOperator = OperatorUtil.Operators.TryGetValue(token?.Str, out Operator op);
                    
            switch (op)
            {
                case Operator.OPENING:
                    opStack.Push(Operator.OPENING);
                    break;
                    
                case Operator.CLOSING:
                    while (opStack.Peek() != Operator.OPENING)
                    {
                        PopOperator(expressionStack, opStack);
                    }

                    opStack.Pop();
                    break;
                    
                default:
                    if (isOperator)
                    {
                        if (prevToken == null 
                            || ExpressionTokenizer.TokenType.IDENTIFIER != prevToken?.Type
                            && ExpressionTokenizer.TokenType.DOUBLE != prevToken?.Type
                            && ExpressionTokenizer.TokenType.INTEGER != prevToken?.Type)
                        {
                            if (op == Operator.MINUS || op == Operator.PLUS)
                            {
                                expressionStack.Push(SubstitutionValue.CreateValue(0));
                            }
                        }
            
                        while (opStack.Count > 0 && opStack.Peek().Priority() >= op.Priority())
                        {
                            PopOperator(expressionStack, opStack);
                        }
            
                        opStack.Push(op);
                    }
                    else
                    {
                        throw new ParseException("Unrecognized operator " + token?.Str);
                    }
                    break;
            }
        }
        
        private static void PopOperator(Stack<IEvaluable> expressionStack, Stack<Operator> opStack)
        {
            Operator current = opStack.Pop();

            if (current == Operator.NOT)
            {
                if (expressionStack.Count < 1)
                {
                    throw new ParseException();
                }
                expressionStack.Push(new OperatorNode(current, null, expressionStack.Pop()));
            }
            else
            {
                if (expressionStack.Count < 2)
                {
                    throw new ParseException();
                }
                expressionStack.Push(new OperatorNode(current, expressionStack.Pop(), expressionStack.Pop()));
            }
        }
        
        public static IEvaluable Parse(string str)
        {
            ExpressionTokenizer tk = new ExpressionTokenizer(str);
            
            Stack<IInstruction> blockStack = new Stack<IInstruction>();
            Stack<IEvaluable> expressionStack = new Stack<IEvaluable>();
            Stack<Operator> opStack = new Stack<Operator>();
            
            ExpressionTokenizer.Token? token = null;
            
            while (true)
            {   
                var prevToken = token;
                token = tk.GetNextToken();

                if (token == null)
                {
                    break;
                }
                
                switch (token?.Type)
                {
                    case ExpressionTokenizer.TokenType.FUNCTION:
                        token = tk.GetNextToken();

                        FunctionDefinition fd;
                        
                        if (token?.Type == ExpressionTokenizer.TokenType.IDENTIFIER)
                        {
                            fd = new FunctionDefinition(token?.Str);

                            token = tk.GetNextToken();
                        }
                        else
                        {
                            fd = new FunctionDefinition();
                        }

                        if (token?.Type == ExpressionTokenizer.TokenType.CALLSTART)
                        {
                            while (token?.Type != ExpressionTokenizer.TokenType.CALLEND)
                            {
                                token = tk.GetNextToken();
                                fd.AddParameter(token?.Str);
                                token = tk.GetNextToken();
                            }

                            token = tk.GetNextToken();
                        }
                            
                        blockStack.Push(fd);

                        if (token?.Type != ExpressionTokenizer.TokenType.BLOCKSTART)
                        {
                            throw new IllegalInstructionException();
                        }
                        
                        break;
                    
                    case ExpressionTokenizer.TokenType.RETURN:
                        blockStack.Push(new ReturnInstruction());
                        break;
                    
                    case ExpressionTokenizer.TokenType.ASSIGNMENT:
                        if (prevToken?.Type == ExpressionTokenizer.TokenType.IDENTIFIER)
                        {
                            expressionStack.Pop();
                            blockStack.Push(new AssignmentInstruction(prevToken?.Str));
                        }
                        else
                        {
                            throw new IllegalInstructionException();
                        }
                        break;
                        
                    case ExpressionTokenizer.TokenType.BLOCKSTART:
                        blockStack.Push(new Block());
                        break;
                    
                    case ExpressionTokenizer.TokenType.BLOCKEND:
                        if (blockStack.Count > 1)
                        {
                            IInstruction finishedBlock = blockStack.Pop();
                            
                            if (blockStack.Peek() is ControlInstruction)
                            {
                                ((ControlInstruction) blockStack.Peek()).AddBody((Block) finishedBlock);
                                finishedBlock = blockStack.Pop();
                            }
                            
                            ((Block) blockStack.Peek()).Push(finishedBlock);
                        }
                        
                        break;
                    
                    case ExpressionTokenizer.TokenType.IF:
                        blockStack.Push(new IfInstruction());
                        break;
                    
                    case ExpressionTokenizer.TokenType.WHILE:
                        blockStack.Push(new WhileInstruction());
                        break;
                    
                    case ExpressionTokenizer.TokenType.IDENTIFIER:
                        expressionStack.Push(new VariableNode(token?.Str));
                        break;
                    
                    case ExpressionTokenizer.TokenType.CALLSTART:
                        if (prevToken?.Type == ExpressionTokenizer.TokenType.IDENTIFIER)
                        {
                            expressionStack.Pop();
                            expressionStack.Push(new FunctionCall(prevToken?.Str));
                        }
                        opStack.Push(Operator.MARK);
                        
                        break;
                        
                    case ExpressionTokenizer.TokenType.SEPARATOR:
                        while (opStack.Count > 0 && opStack.Peek() != Operator.MARK)
                        {
                            PopOperator(expressionStack, opStack);
                        }
                        
                        if (expressionStack.TryPop(out IEvaluable expression1)
                            && expressionStack.Count > 0 && expressionStack.Peek() is FunctionCall fc1)
                        {
                            fc1.AddParameter(expression1);
                        }
                        else
                        {
                            throw new ParseException();
                        }
                        
                        break;
                    
                    case ExpressionTokenizer.TokenType.CALLEND:
                        while (opStack.Count > 0 && opStack.Peek() != Operator.MARK)
                        {
                            PopOperator(expressionStack, opStack);
                        }

                        opStack.Pop();

                        if (prevToken?.Type == ExpressionTokenizer.TokenType.CALLSTART)
                        {
                            break;
                        }
                        
                        expressionStack.TryPop(out IEvaluable expression2);

                        if (expression2 == null)
                        {
                            break;
                        }
                       
                        if (expressionStack.Count > 0 && expressionStack.Peek() is FunctionCall fc2)
                        {
                            fc2.AddParameter(expression2);
                        }
                        else if (blockStack.Peek() is ControlInstruction)
                        {
                            ((ControlInstruction) blockStack.Peek()).AddHead(expression2);
                        }
                        else
                        {
                            throw new ParseException();
                        }
                        
                        break;
                    
                    case ExpressionTokenizer.TokenType.BLOCKSEPARATOR:
                        while (opStack.Count > 0)
                        {
                            PopOperator(expressionStack, opStack);
                        }
                        
                        expressionStack.TryPop(out IEvaluable expression3);
                        
                        if (blockStack.Peek() is RhsInstruction)
                        {
                            RhsInstruction rhs = (RhsInstruction) blockStack.Pop();
                            rhs.SetRhs(expression3);
                            
                            if (blockStack.Peek() is Block)
                            {
                                ((Block) blockStack.Peek()).Push(rhs);
                            }
                            else
                            {
                                throw new ParseException();
                            }
                        }
                        else if (blockStack.Peek() is Block)
                        {
                            ((Block) blockStack.Peek()).Push(expression3);
                        }
                        else
                        {
                            throw new ParseException();
                        }

                        break;
                    
                    case ExpressionTokenizer.TokenType.DOUBLE:
                        expressionStack.Push(SubstitutionValue.CreateValue(double.Parse(token?.Str)));
                        break;
                    
                    case ExpressionTokenizer.TokenType.INTEGER:
                        expressionStack.Push(SubstitutionValue.CreateValue(int.Parse(token?.Str)));
                        break;
                    
                    case ExpressionTokenizer.TokenType.STRING:
                        expressionStack.Push(SubstitutionValue.CreateValue(token?.Str));
                        break;
                    
                    case ExpressionTokenizer.TokenType.OPERATOR:
                        HandleOperator(expressionStack, opStack, token, prevToken);
                        break;
                }
            }
            
            while (opStack.Count > 0)
            {
                PopOperator(expressionStack, opStack);
            }

            if (expressionStack.Count > 0)
            {
                return expressionStack.Pop();
            }

            if (blockStack.Count > 0 && blockStack.Peek() is FunctionDefinition)
            {
                return (FunctionDefinition) blockStack.Peek();
            }

            throw new IllegalInstructionException();
        }
    }
}