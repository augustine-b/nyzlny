using System.Text;

namespace ParserAndInterpreter
{
    public class ExpressionTokenizer
    {
        public enum TokenType
        {
            INTEGER,
            DOUBLE,
            STRING,
            IF,
            WHILE,
            FUNCTION,
            RETURN,
            BLOCKSEPARATOR,
            ASSIGNMENT,
            IDENTIFIER,
            OPERATOR,
            BLOCKSTART,
            BLOCKEND,
            CALLSTART,
            CALLEND,
            SEPARATOR,
        }

        public struct Token
        {
            public TokenType Type { get; }
            public string Str { get; }

            public Token(TokenType type) : this(type, null) {}
            
            public Token(TokenType type, string str)
            {
                Type = type;
                Str = str;
            }
        }
        
        private readonly string _str;
        private int _index;
        
        public ExpressionTokenizer(string str)
        {
            _str = str;
        }

        public Token? GetNextToken()
        {
            while (_index < _str.Length)
            {
                switch (_str[_index])
                {
                    case '{':
                        _index++;
                        return new Token(TokenType.BLOCKSTART);
                    case '}':
                        _index++;
                        return new Token(TokenType.BLOCKEND);
                    case '[':
                        _index++;
                        return new Token(TokenType.CALLSTART);
                    case ']':
                        _index++;
                        return new Token(TokenType.CALLEND);
                    case ',':
                        _index++;
                        return new Token(TokenType.SEPARATOR);
                    case ';':
                        _index++;
                        return new Token(TokenType.BLOCKSEPARATOR);
                }
                
                StringBuilder sb = new StringBuilder();

                if (_str[_index] == '"')
                {
                    _index++;

                    while (_index < _str.Length && _str[_index] != '"')
                    {
                        sb.Append(_str[_index++]);
                    }

                    _index++;

                    return new Token(TokenType.STRING, sb.ToString());
                }
                
                while (_index < _str.Length && OperatorUtil.IsOperatorChar(_str[_index]))
                {
                    sb.Append(_str[_index++]);

                    if (_index == _str.Length || !OperatorUtil.Operators.ContainsKey(sb.ToString() + _str[_index]))
                    {
                        if (sb.ToString() == "=")
                        {
                            return new Token(TokenType.ASSIGNMENT);
                        }
                        
                        return new Token(TokenType.OPERATOR, sb.ToString());
                    }
                }
                
                if (sb.Length > 0)
                {
                    if (sb.ToString() == "=")
                    {
                        return new Token(TokenType.ASSIGNMENT);
                    }
                    
                    return new Token(TokenType.OPERATOR, sb.ToString());
                }

                TokenType numberType = TokenType.INTEGER;
                
                while (_index < _str.Length && (char.IsDigit(_str[_index]) || _str[_index] == '.'))
                {
                    if (_str[_index] == '.')
                    {
                        numberType = TokenType.DOUBLE;
                    }
                    
                    sb.Append(_str[_index++]);
                }

                if (sb.Length > 0)
                {
                    return new Token(numberType, sb.ToString());
                }

                if (char.IsLetter(_str[_index]))
                {
                    while (_index < _str.Length && char.IsLetterOrDigit(_str[_index]))
                    {
                        sb.Append(_str[_index++]);
                    }
                }

                if (sb.Length > 0)
                {
                    if (sb.ToString() == "if")
                    {
                        return new Token(TokenType.IF);
                    }
                    if (sb.ToString() == "while")
                    {
                        return new Token(TokenType.WHILE);                       
                    }
                    if (sb.ToString() == "function")
                    {
                        return new Token(TokenType.FUNCTION);
                    }
                    if (sb.ToString() == "return")
                    {
                        return new Token(TokenType.RETURN);
                    }
                    
                    return new Token(TokenType.IDENTIFIER, sb.ToString());
                }

                _index++;
            }

            return null;
        }
    }
}