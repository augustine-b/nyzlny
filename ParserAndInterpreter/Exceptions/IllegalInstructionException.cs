using System;

namespace ParserAndInterpreter
{
    public class IllegalInstructionException : Exception
    {
        public IllegalInstructionException()
        {    
        }

        public IllegalInstructionException(string message)
            : base(message)
        {
        }

        public IllegalInstructionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}