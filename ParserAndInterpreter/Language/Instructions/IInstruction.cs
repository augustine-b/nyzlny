using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public interface IInstruction
    {
        void Execute(Dictionary<string, IEvaluable> variableBindings);

        IEnumerable<string> GetVariableNames();
    }
}
