using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class VariableNode : IEvaluable
    {
        private readonly string _name;

        public VariableNode(string name)
        {
            _name = name;
        }

        public IEvaluable Evaluate(Dictionary<string, IEvaluable> variableBindings)
        {
            if (variableBindings.TryGetValue(_name, out var val))
            {
                return val;
            }

            throw new VariableNotBoundException("Variable " + _name + " not bound");
        }

        public void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            throw new IllegalInstructionException();
        }

        public IEnumerable<string> GetVariableNames()
        {
            return new List<string>()
            {
                _name
            };
        }
    }
}
