using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public abstract class RhsInstruction : IInstruction
    {
        protected IEvaluable Rhs;

        public void SetRhs(IEvaluable rhs)
        {
            Rhs = rhs;
        }

        public abstract void Execute(Dictionary<string, IEvaluable> variableBindings);

        public IEnumerable<string> GetVariableNames()
        {
            return Rhs.GetVariableNames();
        }
    }
}
