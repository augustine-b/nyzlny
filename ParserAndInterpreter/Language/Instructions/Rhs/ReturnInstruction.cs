using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class ReturnInstruction : RhsInstruction
    {
        public override void Execute(Dictionary<string, IEvaluable> variableBindings)
        {
            if (Rhs != null)
            {
                throw new ReturnValue(Rhs.Evaluate(variableBindings));
            }

            throw new ReturnValue();
        }
    }
}