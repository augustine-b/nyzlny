using System;
using System.Text;

namespace ParserAndInterpreter
{
    public static class Commands
    {
        public static Command GetCommand(string name)
        {
            switch (name)
            {
                case "SVG":
                    return new CommandSVG();
                case "Line":
                    return new CommandLine();
                case "Rect":
                    return new CommandRect();
                case "Polyline":
                    return new CommandPolyline();
                case "Triangle":
                    return new CommandTriangle();
                case "Circle":
                    return new CommandCircle();
                case "Text":
                    return new CommandText();
                case "WindowPane":
                    return new CommandWindowPane();
                case "StrokeColor":
                    return new CommandStrokeColor();
                case "StrokeWidth":
                    return new CommandStrokeWidth();
                case "Fill":
                    return new CommandFill();
                case "AddToPolyline":
                    return new CommandAddToPolyline();
                case "AddToNode":
                    return new CommandAddToNode();
                case "Cos":
                    return new CommandCos();
                case "Sin":
                    return new CommandSin();
                default:
                    return null;
            }
        }

        private static string AsString(IEvaluable[] l, string command, int i)
        {
            if (l[i] is SubstitutionValue sv)
            {
                return sv.GetAsString();
            }

            throw new TypeError("Parameter " + i + " of command " + command
                                + " couldn't be cast to a string");
        }

        private static double AsNumeric(IEvaluable[] l, string command, int i)
        {
            if (l[i] is NumericValue nv)
            {
                return nv.getValue();
            }

            throw new TypeError("Parameter " + i + " of command " + command
                                + " couldn't be cast to a numeric value");
        }

        private static OpeningDirections AsOpeningDirection(IEvaluable[] l, string command, int i)
        {
            if (l[i] is OpeningDirectionValue odv)
            {
                return odv.Val;
            }

            throw new TypeError("Parameter " + i + " of command " + command
                                + " couldn't be cast to an opening direction");
        }

        private class CommandSVG : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                XMLNode node = new XMLNode("svg");
                node.AddAttribute("xmlns", "http://www.w3.org/2000/svg");
                node.AddAttribute("version", "1.1");

                double width = AsNumeric(parameters, "SVG", 0);
                double height = AsNumeric(parameters, "SVG", 1);

                node.AddAttribute("width", width);
                node.AddAttribute("height", height);
                node.AddAttribute("viewBox", 0 + " " + 0 + " " + width + " " + height);

                for (int i = 2; i < parameters.Length; i++)
                {
                    if (parameters[i] is XMLValue xmlv)
                    {
                        node.AddNode(xmlv.Val);
                    }
                    else
                    {
                        throw new TypeError(i, "SVG", ValueType.XMLNODE);
                    }
                }

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandLine : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                XMLNode node = new XMLNode("line");
                node.AddAttribute("x1", AsNumeric(parameters, "Line", 0));
                node.AddAttribute("y1", AsNumeric(parameters, "Line", 1));
                node.AddAttribute("x2", AsNumeric(parameters, "Line", 2));
                node.AddAttribute("y2", AsNumeric(parameters, "Line", 3));

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandRect : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                XMLNode node = new XMLNode("rect");
                node.AddAttribute("x", AsNumeric(parameters, "Rect", 0));
                node.AddAttribute("y", AsNumeric(parameters, "Rect", 1));
                node.AddAttribute("width", AsNumeric(parameters, "Rect", 2));
                node.AddAttribute("height", AsNumeric(parameters, "Rect", 3));

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandPolyline : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                XMLNode node = new XMLNode("polyline");

                StringBuilder points = new StringBuilder();

                for (var i = 0; i < parameters.Length; i += 2)
                {
                    if (i != 0)
                    {
                        points.Append(" ");
                    }

                    points.Append(AsNumeric(parameters, "Polyline", i));
                    points.Append(",");
                    points.Append(AsNumeric(parameters, "Polyline", i + 1));
                }

                node.AddAttribute("points", points.ToString());

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandTriangle : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                double x1 = AsNumeric(parameters, "Triangle", 0);
                double y1 = AsNumeric(parameters, "Triangle", 1);
                double x2 = AsNumeric(parameters, "Triangle", 2);
                double y2 = AsNumeric(parameters, "Triangle", 3);
                double x3 = AsNumeric(parameters, "Triangle", 4);
                double y3 = AsNumeric(parameters, "Triangle", 5);

                XMLNode node = new XMLNode("polyline");
                node.AddAttribute("points",
                    x1 + ", " + y1 + " " +
                    x2 + ", " + y2 + " " +
                    x3 + ", " + y3 + " " +
                    x1 + ", " + y1
                );

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandCircle : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                XMLNode node = new XMLNode("circle");
                node.AddAttribute("cx", AsNumeric(parameters, "Circle", 0));
                node.AddAttribute("cy", AsNumeric(parameters, "Circle", 1));
                node.AddAttribute("r", AsNumeric(parameters, "Circle", 2));

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandText : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                double x = AsNumeric(parameters, "Text", 0);
                double y = AsNumeric(parameters, "Text", 1);
                string text = AsString(parameters, "Text", 2);

                XMLNode node = new XMLNode("text", text);
                node.AddAttribute("x", x);
                node.AddAttribute("y", y);

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandWindowPane : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                OpeningDirections openingDirection = AsOpeningDirection(parameters, "WindowPane", 0);

                double x = AsNumeric(parameters, "WindowPane", 1);
                double y = AsNumeric(parameters, "WindowPane", 2);
                double width = AsNumeric(parameters, "WindowPane", 3);
                double height = AsNumeric(parameters, "WindowPane", 4);

                XMLNode node = new XMLNode("g");
                node.AddAttribute("transform", "translate(" + x + " " + y + ")");
                node.AddAttribute("stroke", "black");
                node.AddAttribute("stroke-width", "1");
                node.AddAttribute("fill", "none");

                XMLNode rect = new XMLNode("rect");
                rect.AddAttribute("x", "0");
                rect.AddAttribute("y", "0");
                rect.AddAttribute("width", width + "");
                rect.AddAttribute("height", height + "");

                node.AddNode(rect);

                // opens to the right
                if ((openingDirection & OpeningDirections.Right) != 0)
                {
                    XMLNode line = new XMLNode("polyline");
                    line.AddAttribute("stroke-dasharray", "3,2");
                    line.AddAttribute("points",
                        "0,0 " +
                        width + "," + (height / 2) + " " +
                        "0," + height
                    );
                    node.AddNode(line);
                }

                // opens to the left
                if ((openingDirection & OpeningDirections.Left) != 0)
                {
                    XMLNode line = new XMLNode("polyline");
                    line.AddAttribute("stroke-dasharray", "3,2");
                    line.AddAttribute("points",
                        width + ",0 " +
                        "0," + (height / 2) + " " +
                        width + "," + height
                    );
                    node.AddNode(line);
                }

                // opens upwards
                if ((openingDirection & OpeningDirections.Top) != 0)
                {
                    XMLNode line = new XMLNode("polyline");
                    line.AddAttribute("stroke-dasharray", "3,2");
                    line.AddAttribute("points",
                        "0," + height + " " +
                        (width / 2) + ",0 " +
                        width + "," + height
                    );
                    node.AddNode(line);
                }

                // opens downwards
                if ((openingDirection & OpeningDirections.Bottom) != 0)
                {
                    XMLNode line = new XMLNode("polyline");
                    line.AddAttribute("stroke-dasharray", "3,2");
                    line.AddAttribute("points",
                        "0,0 " +
                        (width / 2) + "," + height + " " +
                        width + ",0"
                    );
                    node.AddNode(line);
                }

                return SubstitutionValue.CreateValue(node);
            }
        }

        private class CommandFill : CommandG
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                return EvaluateG("fill", parameters);
            }
        }

        private class CommandStrokeColor : CommandG
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                return EvaluateG("stroke", parameters);
            }
        }

        private class CommandStrokeWidth : CommandG
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                return EvaluateG("stroke-width", parameters);
            }
        }

        private class CommandAddToNode : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                if (parameters[0] is XMLValue p1)
                {
                    if (parameters[1] is XMLValue p2)
                    {
                        p1.Val.AddNode(p2.Val);
                        return p1;
                    }

                    throw new TypeError(1, "AddToNode", ValueType.XMLNODE);
                }

                throw new TypeError(0, "AddToNode", ValueType.XMLNODE);
            }
        }

        private class CommandAddToPolyline : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                if (parameters[0] is XMLValue p1)
                {
                    if (p1.Val.NodeType == "polyline")
                    {
                        double x = AsNumeric(parameters, "AddToPolyline", 1);
                        double y = AsNumeric(parameters, "AddToPolyline", 2);

                        string current = p1.Val.GetAttribute("points");
                        p1.Val.AddAttribute("points", current + " " + x + "," + y);

                        return p1;
                    }

                    throw new RuntimeError("Parameter 0 of command AddToPolyline must be a polyline");
                }

                throw new TypeError(0, "AddToNode", ValueType.XMLNODE);
            }
        }

        private class CommandSin : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                if (parameters[0] is NumericValue nv)
                {
                    return SubstitutionValue.CreateValue(Math.Sin(nv.getValue()));
                }

                throw new TypeError();
            }
        }

        private class CommandCos : Command
        {
            public override IEvaluable Evaluate(IEvaluable[] parameters)
            {
                if (parameters[0] is NumericValue nv)
                {
                    return SubstitutionValue.CreateValue(Math.Cos(nv.getValue()));
                }

                throw new IllegalInstructionException();
            }
        }

        public abstract class Command
        {
            public abstract IEvaluable Evaluate(IEvaluable[] parameters);
        }

        public abstract class CommandG : Command
        {
            protected SubstitutionValue EvaluateG(string property, IEvaluable[] parameters)
            {
                XMLNode node = new XMLNode("g");

                node.AddAttribute(property, AsString(parameters, property, 0));

                for (int i = 1; i < parameters.Length; i++)
                {
                    if (parameters[i] is XMLValue xmlv)
                    {
                        node.AddNode(xmlv.Val);
                    }
                    else
                    {
                        throw new TypeError(i, property, ValueType.XMLNODE);
                    }
                }

                return SubstitutionValue.CreateValue(node);
            }
        }
    }
}