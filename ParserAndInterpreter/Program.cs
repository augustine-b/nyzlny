using System.Collections.Generic;

namespace ParserAndInterpreter
{
    public class Program
    {
        static void Main()
        {
            Dictionary<string, IEvaluable> bindings = new Dictionary<string, IEvaluable>();
            bindings.Add("level", SubstitutionValue.CreateValue(5));
            
            // Sierpinski triangle
            string sierpinskiCode = System.IO.File.ReadAllText(@"..\examples\sierpinski.nyl");;
            IEvaluable sierpinskiParsed = Parser.Parse(sierpinskiCode);
            
            XMLValue sierpinski = (XMLValue) sierpinskiParsed.Evaluate(bindings);
            System.IO.File.WriteAllText(@"..\examples\sierpinski.svg", sierpinski.GetAsString());
            
            // Koch snowflake
            string kochCode = System.IO.File.ReadAllText(@"..\examples\koch.nyl");;
            IEvaluable kochParsed = Parser.Parse(kochCode);
            
            XMLValue koch = (XMLValue) kochParsed.Evaluate(bindings);
            System.IO.File.WriteAllText(@"..\examples\koch.svg", koch.GetAsString());
        }
    }
}